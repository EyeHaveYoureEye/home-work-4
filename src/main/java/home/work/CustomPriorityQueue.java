package home.work;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class CustomPriorityQueue<E extends CustomEntity> extends PriorityQueue<E> {

  Map<E, Integer> enterCounter = new HashMap<>();
  Map<E, Integer> pollCounter = new HashMap<>();

  // TODO implement constructors as PriorityQueue with comparator by CustomEntity email
  public CustomPriorityQueue() {

  }

  public CustomPriorityQueue(int initialCapacity) {

  }

  // TODO count how much each element was added / offered to queue.
  // Don't add duplicates to queue
  @Override
  public boolean add(E e) {
    return false;
  }

  // TODO count how much each element was added / offered to queue
  // Don't add duplicates to queue
  @Override
  public boolean offer(E e) {
    return false;
  }

  // TODO count how much each element was polled from queue
  @Override
  public E poll() {
    return null;
  }

  // TODO
  public int getEnterCount(E el) {
    return 0;
  }

  // TODO
  public int getPollCount(E el) {
    return 0;
  }
}


